package client;

import java.rmi.Naming;
import java.util.Scanner;

import server.InterfaceServer;

public class Client 
{
	public static String consultar(int id) throws Exception{
		String result=null;
		String rmiObjectName="rmi://localhost/Data";
		InterfaceServer service= (InterfaceServer) Naming.lookup(rmiObjectName);
		result = service.consult(id);
		
		return result;
	}
	
	public static void main(String[] args) throws Exception {
		String op = null;
		int id = -1;
		Scanner scanner = null;
				do {
					scanner = new Scanner(System.in);
					System.out.println("Buscar datos por Id: ");
					id= scanner.nextInt();
					System.out.println(consultar(id));
					
					System.out.println("Terminar ejecución Si (s) | No (n)");
					op=scanner.next();
					
				}while (op.equals("n"));
		
				scanner.close();
	}	
}
